<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Get the currently authenticated user.
     *
     * @return JsonResponse
     */
    public function current(): JsonResponse
    {
        return response()->json(Auth::user());
    }
}
