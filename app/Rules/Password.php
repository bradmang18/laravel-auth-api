<?php

namespace App\Rules;

use Laravel\Fortify\Rules\Password as FortifyPassword;

class Password extends FortifyPassword
{
    protected $requireUppercase = true;
    protected $requireNumeric = true;
    protected $requireSpecialCharacter = true;
}
